################################################################################
###                                                                          ###
### Created by Martin Genet                                                  ###
###                                                                          ###
### École Polytechnique, Palaiseau, France                                   ###
###                                                                          ###
################################################################################

name: MEC552B-2024

channels:
  - conda-forge
  - defaults

dependencies:
  - fenics=2019.1.0
  - git=2.39.0
  - gnuplot=5.4.3
  - ipydatawidgets=4.3.2
  - ipywidgets=7.6.5
  - itk=5.2.0
  - itkwidgets=0.32.6
  - jupyter=1.0.0
  # MG20240123: Specified all these versions based on the 2023 build to get ipywidgets and itkwidgets to properly interact
  - jupyter_client=7.4.8
  - jupyter_console=6.4.4
  - jupyter_contrib_core=0.4.0
  - jupyter_contrib_nbextensions=0.5.1
  - jupyter_core=5.1.3
  - jupyter_highlight_selected_word=0.2.0
  - jupyter_latex_envs=1.4.6
  - jupyter_nbextensions_configurator=0.4.1
  - jupyter_server=2.0.6
  - jupyter_server_terminals=0.4.3
  # MG20240123: Specified all these versions based on the 2023 build to get ipywidgets and itkwidgets to properly interact
  - jupyterlab=3.5.2
  - matplotlib=3.6.2
  - meshio=5.3.4
  - mpi4py=3.1.3 # MG20220906: To avoid mpi warnings in FEniCS, cf. https://fenicsproject.discourse.group/t/runtimewarning-mpi4py-mpi-file-size-changed-may-indicate-binary-incompatibility-expected-32-from-c-header-got-40-from-pyobject-def-compile-class-cpp-data-mpi-comm-mpi-comm-world/6496 # MG20220906: Note that 3.0.3 leads to a `RuntimeError: Error when importing mpi4py`, but 3.1.3 seems to work.
  - notebook=6.4.6
  - numpy=1.21.5
  - python=3.8.15
  - scipy=1.9.3
  - sympy=1.9 # MG20220906: There is a problem with matrix differentiation with sympy >= 1.10, cf. https://github.com/sympy/sympy/issues/24021 #MG 20230914: Apparently this is being checked, cf. https://github.com/sympy/sympy/pull/25150. Would be cool as upgrading sympy would allow to directly get numpy arrays from sympy array through lambdify (cf. https://github.com/sympy/sympy/issues/19764), simplify code for E2…
  - traitlets=5.6.0 # MG20221220: later traitlets versions seem incompatible with itkwidgets 0.32, cf. https://github.com/InsightSoftwareConsortium/itkwidgets/issues/588
  - vtk=9.0.3 # MG20220628: Apparently there is some kind of conflict between vtk9.1 & libstdc++.so.6 in Ubuntu 20.04…

  - pip
  - pip:
    - git+https://github.com/dolfin-adjoint/pyadjoint.git@2019.1.0
    - dolfin_mech
    - dolfin_warp
    - gmsh==4.9.0 # MG20220912: Apparently 4.6.0 is not available anymore?
    - myPythonLibrary
    - myVTKPythonLibrary
    - pygmsh==6.1.1 # MG20201111: physical groups seem to not work with pygmsh >= 7, see https://github.com/nschloe/pygmsh/issues/397
    - zstandard==0.16.0 # MG20220511: Hiba had issues with newer versions of zstandard…
